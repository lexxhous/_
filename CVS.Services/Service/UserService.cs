﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CVS.Data;
using CVS.Data.Model;

namespace CVS.Services.Service
{
    public class UserService : IUserService
    {
        private CVSContext _context;

        #region Ctors

        public UserService()
             : this(new CVSContext())
        {
        }

        public UserService(CVSContext context)
        {
            _context = context;
        }

        #endregion

        #region Interface

        public IList<User> GetAllUsers()
        {
            return _context.Users.ToList();
        }

        public bool UploadUsers(IList<User> users)
        {
            _context.Users.AddRange(users);
            _context.SaveChanges();

            IList<Relative> relatives = new List<Relative>(); 
            foreach (var user in users)
                if (user.ParentId.HasValue)
                    relatives.Add(new Relative()
                    {
                        User = user,
                        Id = user.Id,
                        ParentId = user.ParentId.Value
                    });
            _context.Relatives.AddRange(relatives);

            if (_context.SaveChanges() > 0)
                return true;
            return false;
        }
        
        #endregion
    }
}
