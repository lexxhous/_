﻿using CVS.Data.Model;
using System.Collections.Generic;

namespace CVS.Services.Service
{
    public interface IUserService
    {
        bool UploadUsers(IList<User> users);
        IList<User> GetAllUsers();
    }
}
