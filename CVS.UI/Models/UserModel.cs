﻿namespace UploadCvs.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateBirth { get; set; }
        public string Gender { get; set; }
        public string ParentId { get; set; }
    }
}