﻿using System;
using CVS.Data.Model;
using UploadCvs.Models;
using System.Globalization;

namespace UploadCvs.Helpers
{
    public static class Converter
    {
        public static User Convert(UserModel model)
        {
            User user = new User();
            user.Id = model.Id;
            user.Name = model.Name.RemoveWhitespace();
            if (!string.IsNullOrEmpty(model.ParentId))
                user.ParentId = int.Parse(model.ParentId);
            user.DateBirth = model.DateBirth.GetDate();
            user.Gender = model.Gender.ToLowerInvariant().RemoveWhitespace().GetGender();

            return user;
        }

        private static string RemoveWhitespace(this string str)
        {
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }
        private static bool? GetGender(this string gender)
        {
            switch(gender)
            {
                case "женский":
                    return false;
                case "ж":
                    return false;
                case "жен":
                    return false;
                case "female":
                    return false;

                case "мужской":
                    return true;
                case "муж":
                    return true;
                case "м":
                    return true;
                case "male":
                    return true;


                default:
                    return null;
            }
        }
        private static DateTime GetDate(this string str)
        {
            CultureInfo ru = new CultureInfo("ru-Ru");
            string format = "dd.MM.yyyy";
            return DateTime.ParseExact(str, format, ru);
        }
    }    
}