﻿using CVS.Data;
using CVS.Data.Model;
using CVS.Services.Service;
using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using UploadCvs.Models;
using UploadCvs.Helpers;

namespace UploadCvs.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController()
        {
            _userService = new UserService();
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase attachmentcsv, bool isFirstRowIsColumnName = true, char delimeter = ';')
        {
            if (attachmentcsv == null)
                return View();

            CsvFileDescription csvFileDescription = new CsvFileDescription
            {
                SeparatorChar = delimeter,
                FirstLineHasColumnNames = isFirstRowIsColumnName
            };
            CsvContext csvContext = new CsvContext();
            StreamReader streamReader = new StreamReader(attachmentcsv.InputStream, Encoding.UTF8);
            IEnumerable<UserModel> userModels = csvContext.Read<UserModel>(streamReader, csvFileDescription);

            IList<User> users = new List<User>();

            foreach (var userModel in userModels)
                users.Add(Converter.Convert(userModel));

            _userService.UploadUsers(users);
            
            return View();
        }

        public ActionResult GetUsers()
        {
            return View(_userService.GetAllUsers());
        }
    }
}