﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVS.Data.Model
{
    [Table("Users")]
    public class User
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Дата рождения")]
        public DateTime DateBirth { get; set; }

        [DisplayName("Пол (м/ж)")]
        public bool? Gender { get; set; }
        
        [ForeignKey("Parent")]
        [DisplayName("Родитель")]
        public int? ParentId { get; set; } = null;
        
        public virtual User Parent { get; set; }
    }
}
