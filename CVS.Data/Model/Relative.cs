﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVS.Data.Model
{
    [Table("Relatives")]
    public class Relative
    {
        [Key, ForeignKey("User")]
        public int Id { get; set; }
        public int ParentId { get; set; }

        public virtual User User { get; set; }
    }
}
