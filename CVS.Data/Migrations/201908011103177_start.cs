namespace CVS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Relatives",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ParentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateBirth = c.DateTime(nullable: false),
                        Gender = c.Boolean(nullable: false),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ParentId)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Relatives", "Id", "dbo.Users");
            DropForeignKey("dbo.Users", "ParentId", "dbo.Users");
            DropIndex("dbo.Users", new[] { "ParentId" });
            DropIndex("dbo.Relatives", new[] { "Id" });
            DropTable("dbo.Users");
            DropTable("dbo.Relatives");
        }
    }
}
