﻿using CVS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVS.Data
{
    public class CVSContext : DbContext
    {
        #region Ctors

        static CVSContext()
        {
            Database.SetInitializer<CVSContext>(null);
        }

        public CVSContext()
            : base("DefaultConnection")
        {

        }

        #endregion

        public DbSet<User> Users { get; set; }
        public DbSet<Relative> Relatives { get; set; }
    }    
}
